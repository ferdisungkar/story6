from django.test import TestCase, LiveServerTestCase
from django.test.client import Client
from django.urls import resolve
from .views import index
from django.apps import apps
from .apps import HomepageConfig
from .forms import status_input
from .models import Post



# Create your tests here.
class Story6Test(TestCase):
    def test_URL_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_template_is_used(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_contains_question(self):
        response = Client().get('')
        response_content = response.content.decode('utf-8')
        self.assertIn("Hello, how are you?", response_content)

    def test_apps(self):
        self.assertEqual(HomepageConfig.name, 'homepage')
        self.assertEqual(apps.get_app_config('homepage').name, 'homepage')

    
    def test_status_form_validation_for_blank(self):
        new_status = status_input(data={'status': ''})
        self.assertFalse(new_status.is_valid())
        self.assertEqual(new_status.errors['user_status'], ["This field is required."])
    
    def test_model_status_return_status_attribute(self):
        new_status = Post.objects.create(user_status='user_status')
        self.assertEqual(str(new_status), new_status.user_status)

    def test_model_can_create_new_status(self):
        new_status = Post.objects.create(user_status='user_status')
        count_all_new_status = Post.objects.all().count()
        self.assertEqual(count_all_new_status,1)

    def test_form_is_valid(self):
        response = self.client.post('', data={'user_status':'Post'})
        response_content = response.content.decode()
        self.assertIn(response_content, 'Post')