from django import forms
from .models import Post

class status_input(forms.Form):
    user_status = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Input your status',
        'type' : 'text',
        'maxlength' : '300',
        'required' : True,
        'style' : 'border:0; width:100%;text-align:center;background:black;color:white;',
        'label' : '',
    }))