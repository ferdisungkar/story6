from django.db import models
from django.utils import timezone
from datetime import datetime, date

# Create your models here.
class Post(models.Model):
    user_status = models.CharField(max_length=300, blank=False)
    published_date = models.DateTimeField(auto_now_add=True, null=True)

    def __str__(self):
        return self.user_status