from django.shortcuts import render, redirect
from .models import Post
from .forms import *

# Create your views here.
def index(request):
    if request.method == "POST":
        form = status_input(request.POST)
        if (form.is_valid()):
            status = Post()
            status.user_status =  form.cleaned_data['user_status']
            status.save()
        return redirect('/')
    else:
        form = status_input()
        status = Post.objects.all().order_by('-published_date')
        response = {'status' : status, 'form' : form}
        return render(request, 'index.html', response)