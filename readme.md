## Author
Ferdi Salim Sungkar

## Link Heroku
[https://ferdisungkar.herokuapp.com](https://ferdisungkar.herokuapp.com)

[![pipeline status](https://gitlab.com/ferdisungkar/story6/badges/master/pipeline.svg)](https://gitlab.com/ferdisungkar/story6/commits/master)

[![coverage report](https://gitlab.com/ferdisungkar/story6/badges/master/coverage.svg)](https://gitlab.com/ferdisungkar/story6/commits/master)